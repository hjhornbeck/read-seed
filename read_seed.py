#!/usr/bin/env python3

###########################################################################
#
# read_seed.py: A quick-and-dirty Python script to extract the seed and 
#  DataVersion from a Minecraft level.dat file.
#
# $ python3 read_seed.py [LEVEL.DAT] {[LEVEL.DAT] ...}
#

##### IMPORTS

import gzip
from sys import argv

##### MAIN

for filename in argv[1:]:

    with gzip.open( filename, 'rb' ) as file:
        blob = file.read()      # it's small enough
    
    seed_bytes = None
    try:
        idx = blob.index( b'RandomSeed' )
        seed_bytes = blob[idx + len(b'RandomSeed'):][:8]
    except:
        pass

    if seed_bytes is None:
        try:
            idx = blob.index( b'seed' )
            seed_bytes = blob[idx + len(b'seed'):][:8]
        except:
            print( f'No RandomSeed in "{filename}", moving to next file.' )
            continue

    seed_int = int.from_bytes( seed_bytes, 'big' )
    if seed_int.bit_length() == 64:
        raw_binary = bin(seed_int)[2:]
        inverted = ''.join(['0' if x == '1' else '1' for x in raw_binary])
        seed_int = -( int( inverted, 2 ) + 1 )

    if b'DataVersion' not in blob:
        dataversion = 'unknown'
    else:
        idx = blob.index( b'DataVersion' )
        version_bytes = blob[idx + len(b'DataVersion'):][:4]
        dataversion = int.from_bytes( version_bytes, 'big' )

    print( f'"{filename}":\t{seed_int}\t{dataversion}' )

