# Read Seed

A quick-and-dirty Python script to extract the random seed and version number from a Minecraft world.

## Usage

python3 read_seed.py [LEVEL.DAT] {[LEVEL.DAT] ...}

```
$ python3 read_seed.py level.dat
"level.dat":    1666681623802787994 1628
```

Currently, you need to manually look up which data version corresponds to which client version, but [the Minecraft Wiki](https://minecraft.wiki/w/Data_version#List_of_data_versions) has an extensive list.
In this case, "1628" corresponds to Java Edition 1.13.1.

## Disclaimers

This script has no ability to read NBT tags or even understand the structure of a Minecraft world save, it's really just a regex. 
It's been tested successfuly on assorted saves ranging from version 1.7.4 to 1.16.4, but carries no guarantee to work, let alone be accurate.
Fortunately this script performs no writes, so you have to work quite hard to do harm with it.